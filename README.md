# FreeAgent Coding Challenge

Thank you for your interest in the FreeAgent Coding Challenge.  This template is a barebones guide to get you started.  Please add any gems, folders, files, etc. you see fit in order to produce a solution you're proud of.

## Coding Challenge Instructions

Please see the INSTRUCTIONS.md file for more information.

## Your Solution Setup and Run Instructions

Please include instructions on how to setup and run your solution here.

## Your Design Decisions

 - My first design decision was to have each piece of logic in its own function with the main function merely moving the information along, this is so it is easy to write tests for

 - Second design decision was that the data load function would use a configuration file to determine where the file came from, with a switch case that can be extended if more data sources were added (e.g. an API call, xml file etc).  New sources should always convert to a JSON file similar to the one provided.

 - Third was to make the base currency a config option as well, to allow dynamically adding the missing entry in the JSON depending on the option selected.

 - I then added a wrapping begin/rescue/end to catch any errors, with additional exceptions throughout the class to provide more detail
