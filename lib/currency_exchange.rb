require 'json'
require 'yaml'
require 'date'

module CurrencyExchange

  # Return the exchange rate between from_currency and to_currency on date as a float.
  # Raises an exception if unable to calculate requested rate.
  # Raises an exception if there is no rate for the date provided.
  def self.rate(date, from_currency, to_currency)
    begin
      ## Step 1: Load Source Config
      source = self.loadConfig('source');
  
       ## Step 2: Load data based on source
      data = self.loadData(source);
    
      ## Step 2: Calculate Rate
      rate = self.calculate(data[date.to_s], from_currency, to_currency);
    rescue
      raise 'Exception: Could not calculate rate'
      return false;
    end
    return rate;
  end

  ## Simple load config function to allow for unit tests
  def self.loadConfig(setting)
    config = YAML.load_file(File.join(__dir__, 'config.yml'));

    return config[setting];
  end

  ## Load file based on source, this method can be ammended to inlude other sources
  def self.loadData(source)
    case source
    when 'json'
      return self.loadJson();
    end
  end

def self.loadJson
  file = File.read(File.join(__dir__, '../data/euro.json'));
  
  return JSON.parse(file)
end

def self.calculate(rates, from_currency, to_currency)
  ## Add the base curreny to rates
  rates[self.loadConfig('base_currency')] = 1.00

  from_rate = rates[from_currency];
  to_rate = rates[to_currency];

  return to_rate / from_rate;
end

end
